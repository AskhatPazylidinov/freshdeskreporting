﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace FreshDeskReporting.Controllers
{
    class RequestController
    {
        private string FdDomain = "financesoft";

        public string ExecuteRequest(string apiPath)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + FdDomain + ".freshdesk.com" + apiPath);
            request.ContentType = "application/json";
            request.Method = "GET";
            request.Headers["Authorization"] = "Basic b0tOTUt1QkRsUEZpQnZVVmdEMjc6WA==";

            string responseBody = String.Empty;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                responseBody = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();

                Console.WriteLine("Status Code: {1} {0}", ((HttpWebResponse)response).StatusCode, (int)((HttpWebResponse)response).StatusCode);
            }
            return responseBody;
        }
        public string CreateNewRequestGetCompanies()
        {
            string apiPath = $"/api/v2/companies";
            return ExecuteRequest(apiPath);
        }

        public string CreateNewRequestWithCompanyId(int newPage, double companyId)
        {
            string apiPath = $"/api/v2/tickets?company_id={companyId}&per_page=100&page={newPage}";
            return ExecuteRequest(apiPath);
        }

        //public string CreateNewRequestForSpecificMonth(int newPage, DateTime start, DateTime end)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{start:yyyy-MM-dd}' AND created_at:<'{end:yyyy-MM-dd}'\"&page={newPage}";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestForCurrentMonthWithStatus(int statusId)
        //{
        //    DateTime datetimeNow = DateTime.Now;
        //    string startedDate = new DateTime(datetimeNow.Year, datetimeNow.Month, 01).ToString("yyyy-MM-dd");
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{startedDate}' AND created_at:<'{datetimeNow:yyyy-MM-dd}' AND status:{statusId}\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestForCurrentWeekWithStatus(int statusId)
        //{
        //    var startWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
        //    var endWeek = startWeek.AddDays(6);
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{startWeek:yyyy-MM-dd}' AND created_at:<'{endWeek:yyyy-MM-dd}' AND status:{statusId}\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestForPreviousDay(int statusId)
        //{
        //    var toDay = DateTime.Now;
        //    var previousDay = toDay.AddDays(-1);
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{previousDay:yyyy-MM-dd}' AND created_at:<'{previousDay:yyyy-MM-dd}' AND status:{statusId}\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestForToDay(int statusId)
        //{
        //    var toDay = DateTime.Now;
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{toDay:yyyy-MM-dd}' AND created_at:<'{toDay:yyyy-MM-dd}' AND status:{statusId}\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestByAgentsAndStatusOfTickets(int statusId, double agentId)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"status:{statusId} AND agent_id:{agentId}\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestByStatusOfTickets(int statusId)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"status:{statusId}\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestGetTotalOfAllTickets()
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"type:'Ошибка/bug' OR type:'Запрос на изменение' OR type:'Вопрос по системе' OR type:'Вопрос по параметризации' OR type:'Запрос нового функционала'\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestWithTicketsType(int newPage, DateTime start, DateTime end, string type)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{start:yyyy-MM-dd}' AND created_at:<'{end:yyyy-MM-dd}' AND type:'{type}'\"&page={newPage}";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestWithTicketsByStatusForPeriod(int newPage, DateTime start, DateTime end, int status)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"created_at:>'{start:yyyy-MM-dd}' AND created_at:<'{end:yyyy-MM-dd}' AND status:{status}\"&page={newPage}";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestWithModule(string module)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"tf:'{module}'\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestWithStatusAndModule(int statusId, string module)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"status: {statusId} AND tf:'{module}'\"";
        //    return ExecuteRequest(apiPath);
        //}

        //public string CreateNewRequestWithClosedStatusAndModule(string module)
        //{
        //    string apiPath = $"/api/v2/search/tickets?query=\"status:4 OR status:5 AND tf:'{module}'\"";
        //    return ExecuteRequest(apiPath);
        //}
    }
}
