﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FreshDeskReporting.Models;

namespace FreshDeskReporting.Controllers
{
    class HtmlController
    {
        private DateTime StartDate { get; set; }
        private DateTime EndDate { get; set; }

        public HtmlController(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public void DeleteHtmlFile(string nameOfReport)
        {
            string path = $"C:\\Projects\\FreshDeskReporting\\{nameOfReport}.html";
            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                fileInf.Delete();
            }
        }

        public async Task CreateHtmlFileBreakdownByTicketsTypeAsync(Dictionary<string, int> report, int total)
        {
            DeleteHtmlFile("Разбивка_по_типам_заявок");
            string bloks = null;
            foreach (var record in report)
            {
                var percent = (double)record.Value / total * 100d;
                percent = Math.Round(percent, 1);
                bloks += "   <tr>\n" +
                         $"<td>{record.Key}</td>\n" +
                         $"<td>{record.Value}</td>\n" +
                         $"<td>{percent}%</td>\n" +
                         $"   </tr>\n";
            }


            StreamWriter streamwriter = new StreamWriter(@"C:\Projects\FreshDeskReporting\Разбивка_по_типам_заявок.html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Отчет по периоду с {StartDate:yyyy MMMM dd} по {EndDate:yyyy MMMM dd}</p>\n" +
                                   $" <table class=\"table\">\n" +
                                   $" <thead>\n" +
                                   $"<tr>\n" +
                                   $"<th>Тип заявки</th>\n" +
                                   $"<th>Количество</th>\n" +
                                   $"<th>Процент от общего кол-ва</th>\n" +
                                   $"</tr>\n" +
                                   $"\n" +
                                   $" </thead >\n" +
                                   $" <tbody>\n" +
                                   $"{bloks}\n" +
                                   "   <tr>\n" +
                                   "<td>Всего</td>\n" +
                                   $"<td>{total}</td>\n" +
                                   "<td>100%</td>\n" +
                                   "   </tr>\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileDistributionOfOpenTicketsByAgentsAsync(List<Agent> agents, Dictionary<int, string> statuses)
        {
            string reportDateTime = DateTime.Now.ToString("yyyy-MM-dd"); 
            int numberOfAgent = 1;
            int total = 0;
            int currentRecord = 0;
            List<int> priorityRecord = new List<int>();
            DeleteHtmlFile($"Распределние_незакрытых_заявок_по_агентам_({DateTime.Now.ToString("yyyy - MM - dd")})");

            string bloks = null;
            foreach (var status in statuses)
            {
                currentRecord++;
                if (status.Value == "Ожидает оценки" || status.Value == "Идет оценка")
                {
                    priorityRecord.Add(currentRecord);
                    bloks += $"<th id=\"priority\">{status.Value}</th>\n";
                }
                else
                {
                    bloks += $"<th>{status.Value}</th>\n";
                }
            }

            string resultBloks = null;
            foreach (var agent in agents)
            {
                currentRecord = 0;
                total = 0;
                resultBloks +="   <tr>\n" +
                             $"<td>{numberOfAgent++}</td>\n" +
                             $"<td>{agent.Name}</td>\n";
                foreach (var ticket in agent.tickets)
                {
                    currentRecord++;
                    total += ticket.Value;
                    if (priorityRecord.Contains(currentRecord))
                    {
                        resultBloks += $"<td id=\"priority\">{ticket.Value}</td>\n";
                    }
                    else
                    {
                        resultBloks += $"<td>{ticket.Value}</td>\n";
                    }
                }
                resultBloks += $"<td>{total}</td>\n" +
                                "   </tr>\n";
            }

            StreamWriter streamwriter = new StreamWriter($"C:\\Projects\\FreshDeskReporting\\Распределние_незакрытых_заявок_по_агентам_({DateTime.Now.ToString("yyyy - MM - dd")}).html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                  ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Распределение незакрытых заявок по агентам ({reportDateTime})<p>\n" +
                                   " <table class=\"table\">\n" +
                                   " <thead>\n" +
                                   "<tr>\n" +
                                   "<th>№п/п</th>\n" +
                                   "<th>Агент</th>\n" +
                                   $"{bloks}\n" +
                                   "<th>Итого</th>\n" +
                                   " </thead >\n" +
                                   " <tbody>\n" +
                                   $"{resultBloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileStatusesForTodayInDynamicsAsync(List<Status> statuses)
        {
            DeleteHtmlFile($"Статусы_на_сегодня_в_динамике_({DateTime.Now.ToString("yyyy-MM-dd")})");
            var totalOfAllStatuses = 0;

            foreach (var status in statuses)
            {
                totalOfAllStatuses += status.Total;
            }

            string bloks = null;
            foreach (var status in statuses)
            {
                var percent = (double)status.Total / totalOfAllStatuses * 100d;
                percent = Math.Round(percent, 1);
                if (status.Name == "Ожидает оценки" || status.Name == "Идет оценка")
                {
                    if (status.CountOfPreviousDayDifference > 0)
                    {
                        bloks +=
                            "   <tr id=\"priority\">\n" +
                            $"<td>{status.Name}</td>\n" +
                            $"<td>{status.Total}</td>\n" +
                            $"<td>{percent}%</td>\n" +
                            $"<td>+{status.CountOfPreviousDayDifference}</td>\n" +
                            $"<td>{status.CountForWeek}</td>\n" +
                            $"<td>{status.CountFromTheBeginningOfTheMonth}</td>\n" +
                            "   </tr>\n";
                    }
                    else
                    {
                        bloks +=
                            "   <tr id=\"priority\">\n" +
                            $"<td>{status.Name}</td>\n" +
                            $"<td>{status.Total}</td>\n" +
                            $"<td>{percent}%</td>\n" +
                            $"<td>{status.CountOfPreviousDayDifference}</td>\n" +
                            $"<td>{status.CountForWeek}</td>\n" +
                            $"<td>{status.CountFromTheBeginningOfTheMonth}</td>\n" +
                            "   </tr>\n";
                    }
                }
                else
                {
                    if (status.CountOfPreviousDayDifference > 0)
                    {
                        bloks +=
                            "   <tr>\n" +
                            $"<td>{status.Name}</td>\n" +
                            $"<td>{status.Total}</td>\n" +
                            $"<td>{percent}%</td>\n" +
                            $"<td>+{status.CountOfPreviousDayDifference}</td>\n" +
                            $"<td>{status.CountForWeek}</td>\n" +
                            $"<td>{status.CountFromTheBeginningOfTheMonth}</td>\n" +
                            "   </tr>\n";
                    }
                    else
                    {
                        bloks +=
                            "   <tr>\n" +
                            $"<td>{status.Name}</td>\n" +
                            $"<td>{status.Total}</td>\n" +
                            $"<td>{percent}%</td>\n" +
                            $"<td>{status.CountOfPreviousDayDifference}</td>\n" +
                            $"<td>{status.CountForWeek}</td>\n" +
                            $"<td>{status.CountFromTheBeginningOfTheMonth}</td>\n" +
                            "   </tr>\n";
                    }
                }
            }

            StreamWriter streamwriter = new StreamWriter($"C:\\Projects\\FreshDeskReporting\\Статусы_на_сегодня_в_динамике_({DateTime.Now.ToString("yyyy-MM-dd")}).html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Статусы на 9:00 утра сегодня ({DateTime.Now.ToString("yyyy - MM - dd")}) в динамике</p>\n" +
                                   " <table class=\"table\">\n" +
                                   " <thead>\n" +
                                   "<tr>\n" +
                                   "<th>Тип заявки</th>\n" +
                                   $"<th>Количество ({totalOfAllStatuses})</th>\n" +
                                   "<th>Процент от общего кол-ва</th>\n" +
                                   "<th>По сравнению с предыдущим днем</th>\n" +
                                   "<th>За неделю</th>\n" +
                                   "<th>За месяц</th>\n" +
                                   "</tr>\n" +
                                   " </thead>\n" +
                                   " <tbody>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n" +
                                   "");
            streamwriter.Close();
        }
       
        public async Task CreateHtmlFileMonthly_DivisionIntoErrors_OtherAndClosedAsync(List<Month> months)
        {
            DeleteHtmlFile($"Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now.ToString("yyyy - MM - dd")})");
            CultureInfo culture = new System.Globalization.CultureInfo("ru-RU");

            string bloks = null;
            foreach (var month in months)
            {
                var monthName = new DateTime(DateTime.Now.Year,month.NumberOfMonth,01);
                bloks += $"<tr>\n" +
                         $"<td>{monthName.ToString("MMM", culture)}</td>\n" +
                         $"<td>{month.CountOfCreatedTickets}</td>\n" +
                         $"<td>{month.CountOfCreatedTicketsWithTypeError}</td>\n" +
                         $"<td>{month.CountOfTicketsWithOtherTypes}</td>\n" +
                         $"<td>{month.CountOfClosedTicketsInThisMonth}</td>\n" +
                         $"<td>{month.Difference}</td>\n" +
                         $"</tr>";
            }

            StreamWriter streamwriter = new StreamWriter($"C:\\Projects\\FreshDeskReporting\\Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now.ToString("yyyy - MM - dd")}).html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  margin: auto;\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "text-align: center;\n" +
                                   "padding-left: 25px;\n" +
                                   "padding-right: 25px;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Статистика за период с 01 января текущего года по последний день месяца последнего отчетного периода ({DateTime.Now.ToString("yyyy - MM - dd")})</p>\n" +
                                   " <table class=\"table\">\n" +
                                   " <thead>\n" +
                                   "<tr>\n" +
                                   "<th>Создано</th>\n" +
                                   $"<th>Всего</th>\n" +
                                   "<th>Ошибки</th>\n" +
                                   "<th>Другое</th>\n" +
                                   "<th>Закрыто в этом месяце</th>\n" +
                                   "<th>Разница</th>\n" +
                                   "</tr>\n" +
                                   " </thead>\n" +
                                   " <tbody>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n" +
                                   "");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileDistributionOfOpenOrdersByClientsAndModulesAsync(List<Company> listOfCompanies , List<string> companies)
        {
            DeleteHtmlFile($"Распределение_незакрытых_заявок_по_клиентам_и_модулям");

            int currentNum = 1;
            string bloks = null;
            foreach (var company in listOfCompanies)
            {
                if (companies.Contains(company.name))
                {
                    var total = 0;
                    bloks += $"<tr>\n" +
                             $"<td>{currentNum++}</td>\n" +
                             $"<td>{company.name}</td>\n" +
                             $"<td>{company.Modules["Кредиты"]}</td>\n" +
                             $"<td>{company.Modules["РКО"]}</td>\n" +
                             $"<td>{company.Modules["АУР"]}</td>\n" +
                             $"<td>{company.Modules["Бухгалтерия"]}</td>\n" +
                             $"<td>{company.Modules["Депозиты/Расчетные счета"]}</td>\n" +
                             $"<td>{company.Modules["Интернет банкинг"]}</td>\n" +
                             $"<td>{company.Modules["Карты"]}</td>\n" +
                             $"<td>{company.Modules["Касса"]}</td>\n" +
                             $"<td>{company.Modules["Клиенты"]}</td>\n" +
                             $"<td>{company.Modules["Комплаенс"]}</td>\n" +
                             $"<td>{company.Modules["Справочные данные"]}</td>\n" +
                             $"<td>{company.Modules["Сервис"]}</td>\n" +
                             $"<td>{company.Modules["Отчеты"]}</td>\n" +
                             $"<td>{total = company.Modules.Values.Sum()}</td>\n" +
                             $"</tr>\n";
                }
            }

            StreamWriter streamwriter = new StreamWriter($"C:\\Projects\\FreshDeskReporting\\Распределение_незакрытых_заявок_по_клиентам_и_модулям.html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  margin: auto;\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "text-align: center;\n" +
                                   "padding-left: 25px;\n" +
                                   "padding-right: 25px;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Распределение незакрытых заявок по клиентам и модулям</p>\n" +
                                   " <table class=\"table\">\n" +
                                   " <thead>\n" +
                                   "<tr>\n" +
                                   "<th>№п/п</th>\n" +
                                   $"<th>Компании</th>\n" +
                                   "<th>Кредиты</th>\n" +
                                   "<th>РКО</th>\n" +
                                   "<th>АУР</th>\n" +
                                   "<th>Бухгалтерия</th>\n" +
                                   "<th>Депозиты,расчетные</th>\n" +
                                   "<th>ИБ</th>\n" +
                                   "<th>Карты</th>\n" +
                                   "<th>Касса</th>\n" +
                                   "<th>Клиенты</th>\n" +
                                   "<th>Комплаенс</th>\n" +
                                   "<th>Справочные данные</th>\n" +
                                   "<th>Сервис</th>\n" +
                                   "<th>Отчеты</th>\n" +
                                   "<th>Итого</th>\n" +
                                   "</tr>\n" +
                                   " </thead>\n" +
                                   " <tbody>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n" +
                                   "");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileModularlyAsync(List<Module> modules)
        {
            DeleteHtmlFile("Помодульно");
            string bloks = null;
            foreach (var module in modules)
            {
                bloks += "   <tr>\n" +
                         $"<td>{module.Percent}%</td>\n" +
                         $"<td>{module.Name}</td>\n" +
                         $"<td>{module.Total}</td>\n" +
                         $"<td>{module.CountOfOpenedStatus}</td>\n" +
                         $"<td>{module.CountOfClosedStatus}</td>\n" +
                         $"<td>{module.CountOfOtherStatus}</td>\n" +
                         $"   </tr>\n";
            }

            StreamWriter streamwriter = new StreamWriter(@"C:\Projects\FreshDeskReporting\Помодульно.html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Помодульно</p>\n" +
                                   $" <table class=\"table\">\n" +
                                   $" <thead>\n" +
                                   $"<tr>\n" +
                                   $"<th>В%</th>\n" +
                                   $"<th>Модули</th>\n" +
                                   $"<th>Всего</th>\n" +
                                   $"<th>Открыто</th>\n" +
                                   $"<th>Закрыто</th>\n" +
                                   $"<th>Другое</th>\n" +
                                   $"</tr>\n" +
                                   $"\n" +
                                   $" </thead >\n" +
                                   $" <tbody>\n" +
                                   "   <tr>\n" +
                                   "<td>100%</td>\n" +
                                   $"<td>-</td>\n" +
                                   $"<td>{modules.Sum(x=>x.Total)}</td>\n" +
                                   $"<td>{modules.Sum(x=>x.CountOfOpenedStatus)}</td>\n" +
                                   $"<td>{modules.Sum(x => x.CountOfClosedStatus)}</td>\n" +
                                   $"<td>{modules.Sum(x => x.CountOfOtherStatus)}</td>\n" +
                                   "   </tr>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n");
            streamwriter.Close();
        }
    }
}
