﻿using FreshDeskReporting.DistributionReports;
using FreshDeskReporting.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using File = System.IO.File;

namespace FreshDeskReporting.Controllers
{
    class TelegramBotController
    {
        protected static string token { get; } = "1898970636:AAHade0hLsSI2gSbFnq2KqX82Uvde5u9ciI";
        protected static TelegramBotClient client;

        private StateOfBot stateOfBot;
        private SelectedAct selectedAct;

        private string listOfFunctions = String.Empty;
        protected List<string> reports = new List<string>()
        {
            "Разбивка_по_типам_заявок",
            $"Распределние_незакрытых_заявок_по_агентам_({DateTime.Now.ToString("yyyy - MM - dd")})",
            $"Статусы_на_сегодня_в_динамике_({DateTime.Now.ToString("yyyy-MM-dd")})",
            $"Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now.ToString("yyyy - MM - dd")})",
            $"Распределение_незакрытых_заявок_по_клиентам_и_модулям",
            "Помодульно",
        };
        protected static Dictionary<long, string> Employees = new Dictionary<long, string>();
        private List<string> Functions { get; set; } = new List<string>()
        {
            "1-Создать Отчет",
            "2-Подписать на рассылку Отчетов",
            "3-Отключить рассылку отчетов"
        };

        public TelegramBotController()
        {
            foreach (var function in Functions)
            {
                listOfFunctions += $"{function}\n";
            }
        }

        public void Start()
        {
            ReportsSheduler.Start();
            stateOfBot = StateOfBot.SelectOfFunction;
            client = new TelegramBotClient(token);
            client.StartReceiving();
            client.OnMessage += OnMessageHandler;
            while (true)
            {
                Task.Delay(5000);
            }
            //client.StopReceiving();
        }

        private async void OnMessageHandler(object sender, MessageEventArgs e)
        {
            var msg = e.Message;
            if (msg.Text == null)
            {
                return;
            }

            switch ((int)stateOfBot)
            {
                case 1:
                    await client.SendTextMessageAsync(msg.Chat.Id, $"Выберите номер действия!\n{listOfFunctions}", replyMarkup: ClearButtons());
                    stateOfBot = StateOfBot.SelectOfAct;
                    break;
                case 2:
                    await SelectFunction(msg);
                    stateOfBot = StateOfBot.Executing;
                    break;
                case 3:
                    await ExecutingAct(msg);
                    break;
            }
        }

        private async Task SelectFunction(Message msg)
        {
            string result = msg.Text.Replace("/", "");
            switch (result)
            {
                case "1":
                    await client.SendTextMessageAsync(msg.Chat.Id, "Выберите тип отчета!", replyMarkup: GetButtons());
                    selectedAct = SelectedAct.CreateReports;
                    break;
                case "2":
                    await client.SendTextMessageAsync(msg.Chat.Id, "Укажите свою фамилию!", replyMarkup: ClearButtons());
                    selectedAct = SelectedAct.EnableDistributionReports;
                    break;
                case "3":
                    await DeleteFromListOfEmployees(msg);
                    break;
                default:
                    await client.SendTextMessageAsync(msg.Chat.Id, $"Выберите верное действие!\n{listOfFunctions}", replyMarkup: ClearButtons());
                    stateOfBot = StateOfBot.SelectOfAct;
                    break;
            }
        }

        private async Task ExecutingAct(Message msg)
        {
            switch ((int)selectedAct)
            {
                case 1:
                    await CreateReport(msg);
                    break;
                case 2:
                    await AddToListOfEmployees(msg);
                    break;
            }

        }

        private async Task DeleteFromListOfEmployees(Message msg)
        {
            if (Employees.ContainsKey(msg.Chat.Id))
            {
                Employees.Remove(msg.Chat.Id);
                await client.SendTextMessageAsync(msg.Chat.Id, "Вы отключены от рассылки!", replyMarkup: ClearButtons());
            }
            else
            {
                await client.SendTextMessageAsync(msg.Chat.Id, "Рассылка уже отключена!", replyMarkup: ClearButtons());
            }
            stateOfBot = StateOfBot.SelectOfFunction;
        }

        private async Task AddToListOfEmployees(Message msg)
        {
            if (!Employees.ContainsKey(msg.Chat.Id))
            {
                Employees.Add(msg.Chat.Id, msg.Text);
                await client.SendTextMessageAsync(msg.Chat.Id, "Вы подписаны на рассылку!", replyMarkup: ClearButtons());
                stateOfBot = StateOfBot.SelectOfFunction;

                string writePath = $"C:\\Projects\\FreshDeskReporting\\Employees.txt";
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.Write($"{msg.Chat.Id}/");
                }
            }
            else
            {
                await client.SendTextMessageAsync(msg.Chat.Id, "Вы уже подписаны на рассылку отчетов!", replyMarkup: ClearButtons());
                stateOfBot = StateOfBot.SelectOfFunction;
            }
        }

        private async Task CreateReport(Message msg)
        {
            await client.SendTextMessageAsync(msg.Chat.Id, "Ожидайте, идет формирование отчета/отчетов!", replyMarkup: ClearButtons());
            string result = await SelectReportType(msg.Text);

            if (result == null)
            {
                return;
            }

            if (result == "Все типы")
            {
                foreach (var report in reports)
                {
                    await SendCreatedReport(report, msg);
                }
            }
            else if (result == "Error")
            {
                await client.SendTextMessageAsync(msg.Chat.Id, "Тип отчета неверный!", replyMarkup: GetButtons());
            }
            else
            {
                await SendCreatedReport(result, msg);
            }
            stateOfBot = StateOfBot.SelectOfFunction;
        }

        private async Task SendCreatedReport(string report, Message msg)
        {
            string path = $"C:\\Projects\\FreshDeskReporting\\{report}.html";
            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                using (var sendFileStream = File.Open(path, FileMode.Open))
                {
                    await client.SendDocumentAsync(msg.Chat.Id, new InputOnlineFile(sendFileStream, path));
                }
            }
            await client.SendTextMessageAsync(msg.Chat.Id, "Отчет сформирован!", replyMarkup: ClearButtons());
        }

        protected IReplyMarkup ClearButtons()
        {
            return new ReplyKeyboardRemove();
        }

        private IReplyMarkup GetButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>()
                {
                    new List<KeyboardButton>()
                    {
                        new KeyboardButton("Распределение незакрытых заявок по агентам"),
                    },

                    new List<KeyboardButton>()
                    {
                    new KeyboardButton("Статусы на 9:00 утра сегодня (дд_мм_гггг) в динамике"),
                },
                    new List<KeyboardButton>()
                    {
                        new KeyboardButton("Распределение незакрытых заявок по клиентам и модулям"),
                    },
                    new List<KeyboardButton>()
                    {
                        new KeyboardButton("Статистика за период с 01 января текущего года по последний день месяца последнего отчетного периода"),
                    },
                    new List<KeyboardButton>()
                    {
                        new KeyboardButton("Помодульно"),
                    },
                    new List<KeyboardButton>()
                    {
                        new KeyboardButton("Разбивка по типам заявок"),
                    },
                    new List<KeyboardButton>()
                    {
                        new KeyboardButton("Все типы"),
                    },

                }
            };
        }

        private async Task<string> SelectReportType(string type)
        {
            var report = new ReportsController();
            switch (type)
            {
                case "Распределение незакрытых заявок по агентам":
                    await report.CreateReportDistributionOfOpenTicketsByAgentsAsync();
                    return $"Распределние_незакрытых_заявок_по_агентам_({DateTime.Now.ToString("yyyy - MM - dd")})";

                case "Статусы на 9:00 утра сегодня (дд_мм_гггг) в динамике":
                    await report.CreateReportStatusesForTodayInDynamicsAsync();
                    return $"Статусы_на_сегодня_в_динамике_({DateTime.Now.ToString("yyyy-MM-dd")})";

                case "Распределение незакрытых заявок по клиентам и модулям":
                    await report.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync();
                    return $"Распределение_незакрытых_заявок_по_клиентам_и_модулям";

                case "Статистика за период с 01 января текущего года по последний день месяца последнего отчетного периода":
                    await report.Monthly_DivisionIntoErrors_OtherAndClosedAsync();
                    return $"Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now.ToString("yyyy - MM - dd")})";

                case "Помодульно":
                    await report.CreateReportModularlyAsync();
                    return "Помодульно";

                case "Разбивка по типам заявок":
                    await report.CreateReportBreakdownByTicketsTypeAsync();
                    return "Разбивка_по_типам_заявок";

                case "Все типы":
                    await CreateAllReports();
                    return "Все типы";
                default:
                    return "Error";
            }
        }

        protected async Task CreateAllReports()
        {
            var report = new ReportsController();
            Task t1 = Task.Run(() => report.CreateReportDistributionOfOpenTicketsByAgentsAsync());
            Task t2 = Task.Run(() => report.CreateReportBreakdownByTicketsTypeAsync());
            Task t3 = Task.Run(() => report.CreateReportStatusesForTodayInDynamicsAsync());
            Task t4 = Task.Run(() => report.CreateReportModularlyAsync());
            Task t5 = Task.Run(() => report.Monthly_DivisionIntoErrors_OtherAndClosedAsync());
            Task t6 = Task.Run(() => report.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync());
            await Task.WhenAll(new[] { t1, t2, t3, t4, t5, t6 });
        }
    }
}
