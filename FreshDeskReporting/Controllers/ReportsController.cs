﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using FreshDeskReporting.Models;

namespace FreshDeskReporting.Controllers
{
    class ReportsController
    {
        private static DateTime dateNow = DateTime.Now;
        private static DateTime startDate = new DateTime(dateNow.Year, 01, 01);
        private static int endMonth = Math.Max(1, dateNow.Month - 1);
        private static DateTime endDate = new DateTime(dateNow.Year, endMonth, DateTime.DaysInMonth(dateNow.Year, endMonth)).AddDays(1);

        private HtmlController HtmlController { get; set; }
        private RequestController RequestController { get; set; }

        List<Agent> agents = new List<Agent>()
        {
            new Agent(){id = 17034346764, Email = "artem.chupahin@financesoft.kg", Name = "Артем Чупахин"},
            new Agent(){id = 17030796175, Email = "aida.abykeeva@financesoft.kg", Name = "Аида Абыкеева"},
            new Agent(){id = 17040466609, Email = "gulzana.esenalieva@financesoft.kg", Name = "Гульзана Эсеналиева"},
            new Agent(){id = 17041743609, Email = "joomart.sharabidinov@financesoft.kg", Name = "Жоомарт Шарабидинов"},
            new Agent(){id = 17018844231, Email = "meerim.nurlantbekova@financesoft.kg", Name = "Мээрим Нурлантбекова"},
            new Agent(){id = 17037583798, Email = "ilyas.djienbaev@financesoft.kg", Name = "Ильяс Джиенбаев"}
        };

        Dictionary<int, string> statuses = new Dictionary<int, string>()
        {
            {2,"Открыто"},
            {3,"В ожидании"},
            {15,"Ожидает оценки"},
            {12,"Идет оценка"},
            {13,"Оценка отправлена"},
            {9,"Отказ"},
            {14,"Заказ подтвержден"},
            {10,"В работе"},
            {8,"Передано на проверку"},
            {16,"Feedback от клиента"},
            {4,"Решено"},
            {11,"В очереди на поставку"},
            {5,"Закрыто"},
        };

        List<string> modules = new List<string>()
        {
            "Кредиты",
            "РКО",
            "АУР",
            "Бухгалтерия",
            "Депозиты/Расчетные счета",
            "Интернет банкинг",
            "Карты",
            "Касса",
            "Клиенты",
            "Комплаенс",
            "Справочные данные",
            "Сервис",
            "Отчеты",
        };

        public ReportsController()
        {
            HtmlController = new HtmlController(startDate, endDate);
            RequestController = new RequestController();
        }

        public List<Ticket> GetListOfTickets()
        {
            var companies = new List<string>()
            {
                "ЗАО Компаньон Банк",
                "OAO \"Дос-Кредобанк\"",
                "ЗАО Финка Банк",
                "Капитал банк",
                "ЗАО АКБ \"ТОЛУБАЙ\"",
                "ЗАО Кыргызско-Швейцарский банк",
                "Элет Капитал",
                "ОАО «Инвестиционный банк «Чанг Ан»",
            };
            List<Ticket> listOfTickets = new List<Ticket>();

            var responseBody = RequestController.CreateNewRequestGetCompanies();
            var listOfCompany = JsonSerializer.Deserialize<List<Company>>(responseBody);

            foreach (var company in listOfCompany)
            {
                if (companies.Contains(company.name))
                {
                    var page = 1;
                    do
                    {
                        responseBody = RequestController.CreateNewRequestWithCompanyId(page, company.id);
                        listOfTickets.AddRange(JsonSerializer.Deserialize<List<Ticket>>(responseBody));

                        page++;
                    } while (responseBody != "[]");
                }
            }

            //var jsonString = JsonSerializer.Serialize(listOfTickets);
            //File.WriteAllText($"C:\\Projects\\FreshDeskReporting\\tickets.json", jsonString);

            return listOfTickets;
        }

        public async Task CreateReportBreakdownByTicketsTypeAsync()
        {
            Dictionary<string, int> globalReport = new Dictionary<string, int>()
            {
                { "Ошибка/bug", 0},
                { "Запрос на изменение",0},
                { "Вопрос по системе",0},
                { "Запрос нового функционала",0},
                { "Вопрос по параметризации",0}
            };

            int countOfRecordsInGlobalReport = 0;

            var listOfTickets = GetListOfTickets();

            var keys = globalReport.Keys.ToList();

            for (int i = 0; i < 5; i++)
            {
                var key = keys[i];
                globalReport[key] = listOfTickets.Where(x => x.type == key
                                                              && x.created_at >= startDate
                                                              && x.created_at <= endDate).Count();

                countOfRecordsInGlobalReport += globalReport[key];
            }

            await HtmlController.CreateHtmlFileBreakdownByTicketsTypeAsync(globalReport, countOfRecordsInGlobalReport);
        }

        public async Task CreateReportDistributionOfOpenTicketsByAgentsAsync()
        {
            var listOfTickets = GetListOfTickets();

            var reportByAgents = agents;
            foreach (var agent in agents)
            {
                foreach (var status in statuses)
                {
                    if (!agent.tickets.ContainsKey(status.Value))
                    {
                        agent.tickets.Add(status.Value, listOfTickets.Where(x => x.responder_id == agent.id && x.status == status.Key).Count());
                    }
                }
            }

            await HtmlController.CreateHtmlFileDistributionOfOpenTicketsByAgentsAsync(reportByAgents, statuses);
        }

        public async Task CreateReportStatusesForTodayInDynamicsAsync()
        {
            var listOfTickets = GetListOfTickets();

            List<Status> listOfStatuses = new List<Status>();

            foreach (var recordOfStatus in statuses)
            {
                var status = new Status() { Name = recordOfStatus.Value, Total = listOfTickets.Where(x => x.status == recordOfStatus.Key).Count() };


                status.CountFromTheBeginningOfTheMonth = listOfTickets.Where(x => x.status == recordOfStatus.Key
                                                                                && x.created_at >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01)
                                                                                && x.created_at <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(dateNow.Year, dateNow.Month))).Count();

                var startWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                status.CountForWeek = listOfTickets.Where(x => x.status == recordOfStatus.Key
                                                               && x.created_at >= startWeek
                                                               && x.created_at <= startWeek.AddDays(7)).Count();

                var previousDay = listOfTickets.Where(x => x.status == recordOfStatus.Key
                                                           && x.created_at.Date == dateNow.AddDays(-1).Date).Count();

                var toDay = listOfTickets.Where(x => x.status == recordOfStatus.Key
                                                     && x.created_at.Date == dateNow.Date).Count();

                status.CountOfPreviousDayDifference = toDay - previousDay;

                listOfStatuses.Add(status);
            }

            await HtmlController.CreateHtmlFileStatusesForTodayInDynamicsAsync(listOfStatuses);
        }

        public async Task Monthly_DivisionIntoErrors_OtherAndClosedAsync()
        {
            var listOfTickets = GetListOfTickets();


            var start = new DateTime(DateTime.Now.Year, 01, 01);

            List<Month> reportsByMonth = new List<Month>();

            do
            {
                var end = new DateTime(DateTime.Now.Year, start.Month, DateTime.DaysInMonth(DateTime.Now.Year, start.Month));

                var month = new Month();

                month.NumberOfMonth = start.Month;
                month.CountOfCreatedTickets = listOfTickets.Where(x => x.created_at > start
                                                                      && x.created_at <= end
                                                                          .AddDays(1)).Count();

                month.CountOfCreatedTicketsWithTypeError = listOfTickets.Where(x => x.type == "Ошибка/bug" && x.created_at > start
                                                                                    && x.created_at <= end
                                                                                        .AddDays(1)).Count();
                month.CountOfTicketsWithOtherTypes =
                    month.CountOfCreatedTickets - month.CountOfCreatedTicketsWithTypeError;

                month.CountOfClosedTicketsInThisMonth =
                    listOfTickets.Where(x => x.status == 4 && x.created_at > start
                                                           && x.created_at <= end
                                                               .AddDays(1)).Count() + listOfTickets.Where(x => x.status == 5 && x.created_at > start
                                                                                                                             && x.created_at <= end
                                                                                                                                 .AddDays(1)).Count();
                month.Difference = month.CountOfCreatedTickets - month.CountOfClosedTicketsInThisMonth;

                month.PercentOfTicketsWithTypeError =
                    Math.Round((double)month.CountOfCreatedTicketsWithTypeError / month.CountOfCreatedTickets * 100,
                        1);
                month.PercentOfTicketsWithOtherType =
                    Math.Round((double)month.CountOfTicketsWithOtherTypes / month.CountOfCreatedTickets * 100, 1);

                reportsByMonth.Add(month);

                start = new DateTime(DateTime.Now.Year, start.Month + 1, 01);

            } while (start.Month != DateTime.Now.Month);

            await HtmlController.CreateHtmlFileMonthly_DivisionIntoErrors_OtherAndClosedAsync(reportsByMonth);
        }

        public async Task CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync()
        {
            int nullsModules = 0;

            var companies = new List<string>()
            {
                "ЗАО Компаньон Банк",
                "OAO \"Дос-Кредобанк\"",
                "ЗАО Финка Банк",
                "Капитал банк",
                "ЗАО АКБ \"ТОЛУБАЙ\"",
                "ЗАО Кыргызско-Швейцарский банк",
                "Элет Капитал",
                "ОАО «Инвестиционный банк «Чанг Ан»",
            };

            var responseBody = RequestController.CreateNewRequestGetCompanies();
            var listOfCompany = JsonSerializer.Deserialize<List<Company>>(responseBody);
            foreach (var company in listOfCompany)
            {
                if (companies.Contains(company.name))
                {
                    var page = 1;
                    do
                    {
                        responseBody = RequestController.CreateNewRequestWithCompanyId(page, company.id);
                        var listOfTickets = JsonSerializer.Deserialize<List<Ticket>>(responseBody);

                        foreach (var ticket in listOfTickets)
                        {
                            if (ticket.status != 4 && ticket.status != 5 && ticket.type != null)
                            {
                                if (ticket.custom_fields.tf == null)
                                {
                                    nullsModules++;
                                }
                                else if (!company.Modules.ContainsKey(ticket.custom_fields.tf))
                                {
                                    continue;
                                }
                                else
                                {
                                    company.Modules[ticket.custom_fields.tf]++;
                                }
                            }
                        }

                        page++;
                    } while (responseBody != "[]");
                }
            }

            await HtmlController.CreateHtmlFileDistributionOfOpenOrdersByClientsAndModulesAsync(listOfCompany, companies);
        }

        public async Task CreateReportModularlyAsync()
        {
            List<Module> listOfModules = new List<Module>();

            var listOfTickets = GetListOfTickets();

            foreach (var module in modules)
            {
                if (module != "Отчеты")
                {
                    var moduleRecord = new Module
                    {
                        Name = module,
                        Total = listOfTickets.Count(x => x.custom_fields.tf == module),
                        CountOfOpenedStatus = listOfTickets.Count(x => x.status == 2 && x.custom_fields.tf == module),
                        CountOfClosedStatus = listOfTickets.Count(x => x.status == 4 || x.status == 5 && x.custom_fields.tf == module)
                    };

                    moduleRecord.CountOfOtherStatus = (moduleRecord.Total -
                                                       (moduleRecord.CountOfClosedStatus +
                                                        moduleRecord.CountOfOpenedStatus));

                    // moduleRecord.Percent = Math.Round(((double)moduleRecord.Total / listOfTickets.Count) * 100d, 1);

                    listOfModules.Add(moduleRecord);
                }
            }

            await HtmlController.CreateHtmlFileModularlyAsync(listOfModules);
        }
    }
}

