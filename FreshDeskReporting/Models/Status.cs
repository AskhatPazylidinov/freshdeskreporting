﻿using System;

namespace FreshDeskReporting.Models
{
    class Status
    {
        public string Name { get; set; }

        public int Total { get; set; }

        public int CountOfPreviousDayDifference { get; set; }

        public int CountForWeek { get; set; }

        public int CountFromTheBeginningOfTheMonth { get; set; }
    }
}
