﻿using System.Collections.Generic;

namespace FreshDeskReporting.Models
{
    class Agent
    {
        public double id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public Dictionary<string, int> tickets { get; set; } = new Dictionary<string, int>();
    }
}
