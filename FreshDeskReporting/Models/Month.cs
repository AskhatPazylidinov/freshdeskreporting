﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreshDeskReporting.Models
{
    class Month
    {
        public int NumberOfMonth { get; set; }

        public int CountOfCreatedTickets { get; set; }

        public int CountOfCreatedTicketsWithTypeError { get; set; }

        public double PercentOfTicketsWithTypeError { get; set; }

        public int CountOfTicketsWithOtherTypes { get; set; }

        public double PercentOfTicketsWithOtherType { get; set; }

        public int CountOfClosedTicketsInThisMonth { get; set; }

        public int Difference { get; set; }
    }
}
