﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreshDeskReporting.Models
{
    class Company
    {
        public double id { get; set; }

        public string name { get; set; }

        public Dictionary<string, int> Modules { get; set; } = new Dictionary<string, int>()
        {
            {"Кредиты",0 },
            {"РКО",0 },
            {"АУР",0 },
            {"Бухгалтерия",0 },
            {"Депозиты/Расчетные счета",0 },
            {"Интернет банкинг",0 },
            {"Карты",0 },
            {"Касса",0 },
            {"Клиенты",0 },
            {"Комплаенс",0 },
            {"Справочные данные",0 },
            {"Сервис",0 },
            {"Отчеты",0 },
        };
    }
}
