﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Net;
using System.Text;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Xml.Schema;
using FreshDeskReporting.Controllers;
using FreshDeskReporting.Models;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using FreshDeskReporting.DistributionReports;

namespace FreshDeskReporting
{
    class Program
    {
        static void Main(string[] args)
        {
            //if (args.Any())
            //{
            //    string type = null;
            //    string startDate = null;
            //    string endDate = null;
            //    foreach (var i in args)
            //    {
            //        if (i.ToLower().Contains("startdate"))
            //        {
            //            startDate = i.Substring(10);
            //        }
            //        else if (i.ToLower().Contains("enddate"))
            //        {
            //            endDate = i.Substring(8);
            //        }
            //        else if (i.ToLower().Contains("распределние незакрытых заявок по клиентам"))
            //        {
            //            type = i.Substring(5);
            //        }
            //    }

            //    Console.WriteLine(type);
            //    Console.WriteLine(startDate);
            //    Console.WriteLine(endDate);
            //}

            /*await CreateAllReports();*/

            var TelegramBot = new TelegramBotController();
            TelegramBot.Start();
        }

        static async Task CreateAllReports()
        {
            var report = new ReportsController();
            Task t1 = Task.Run(() => report.CreateReportDistributionOfOpenTicketsByAgentsAsync());
            Task t2 = Task.Run(() => report.CreateReportBreakdownByTicketsTypeAsync());
            Task t3 = Task.Run(() => report.CreateReportStatusesForTodayInDynamicsAsync());
            Task t4 = Task.Run(() => report.CreateReportModularlyAsync());
            Task t5 = Task.Run(() => report.Monthly_DivisionIntoErrors_OtherAndClosedAsync());
            Task t6 = Task.Run(() => report.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync());
            await Task.WhenAll(new[] { t1, t2, t3, t4, t5, t6 });
        }
    }
}
