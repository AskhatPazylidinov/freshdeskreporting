﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreshDeskReporting.Enums
{
    enum SelectedAct
    {
        CreateReports = 1,
        EnableDistributionReports = 2,
    }
}
