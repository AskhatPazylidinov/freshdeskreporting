﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreshDeskReporting.Enums
{
    enum StateOfBot
    {
        SelectOfFunction = 1,
        SelectOfAct = 2,
        Executing = 3
    }
}
