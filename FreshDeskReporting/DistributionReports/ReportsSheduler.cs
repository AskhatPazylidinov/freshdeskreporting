﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Text;

namespace FreshDeskReporting.DistributionReports
{
    public class ReportsSheduler
    {
        public static async void Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<DistributionReports>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithCronSchedule("0 0 9 ? * MON,TUE,WED,THU,FRI *")
                .Build();                        

            await scheduler.ScheduleJob(job, trigger);        
        }
    }
}
