﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshDeskReporting.Controllers;
using Quartz;
using Telegram.Bot.Types.InputFiles;

namespace FreshDeskReporting.DistributionReports
{
    class DistributionReports : TelegramBotController, IJob
    {
        string writePath = $"C:\\Projects\\FreshDeskReporting\\Employees.txt";
        public async Task Execute(IJobExecutionContext context)
        {
            List<string> list = new List<string>(); 
            using (StreamReader sr = new StreamReader(writePath))
            {
                var chek = sr.ReadToEnd();
                list = chek.Split('/').ToList();
            }

            await CreateAllReports();
            foreach (var employee in list)
            {
                foreach (var report in reports)
                {
                    string path = $"C:\\Projects\\FreshDeskReporting\\{report}.html";
                    FileInfo fileInf = new FileInfo(path);
                    if (fileInf.Exists)
                    {
                        using (var sendFileStream = File.Open(path, FileMode.Open))
                        {
                            await client.SendDocumentAsync(employee, new InputOnlineFile(sendFileStream, path));
                        }
                    }
                    await client.SendTextMessageAsync(employee, "Автоматическая рассылка отчетов!", replyMarkup: ClearButtons());
                }
            }
        }
    }
}
